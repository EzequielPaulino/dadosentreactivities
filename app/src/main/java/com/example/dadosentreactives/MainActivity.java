package com.example.dadosentreactives;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;


 public class MainActivity extends AppCompatActivity {

     private EditText nome;
     private EditText idade;
     private EditText altura;

     private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nome = (EditText) findViewById(R.id.nome);
        idade = (EditText) findViewById(R.id.idade);
        altura = (EditText) findViewById(R.id.altura);
        button = (Button) findViewById(R.id.button);

    }
}